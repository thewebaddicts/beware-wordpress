<div class="menu-spacer"></div>
<?php /* Template Name: What is CYBEX  */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>

<?php $about = get_fields(get_the_ID());  ?>
<?php global $post;  ?>




<div class="products products-about">

    <div class="page-banner" style="background-image: url('<?php echo $about['background_image']; ?>');">
        <div class="overlay"></div>
    </div>

    <div class="content">

        <div class="pb-5">

            <h3 class="font-weight-bold mb-3"><?php echo $post->post_title; ?></h3>

            <div class="breadcrumbs mb-5">
                <a href="/"><span>Homepage</span></a>
                <span class="mx-2">/</span>
                <a ><span>Products</span></a>
                <span class="mx-2">/</span>
                <a ><span><?php echo $post->post_title; ?></span></a>
            </div>

            <div class="row py-5">
                <div class="col-lg-6">
                    <div class="ratio-3-2 contain" style="background-image: url('<?php echo $about['introduction']['image']; ?>')">
                    </div>
                </div>
                <div class="col-lg-6 d-flex justify-content-center align-items-center">
                    <div class="d-flex justify-content-center w-75">
                        <div>
                            <h4 class="font-weight-bold mb-3"><?php echo $about['introduction']['label']; ?></h4>
                            <div class="paragraph mb-4 t-opacity-75"><?php echo $about['introduction']['text']; ?></div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if(isset($about['purposes'] ) && sizeof( $about['purposes'])>0 ) { ?>
                    <div class="purposes py-5">
                        <h4 class="font-weight-bold mb-3"><?php echo $about['purposes_title']; ?></h4>
                        <div class="cloud py-5">
                            <div class="list-container">
                                <div class="row">
                                    <?php foreach ($about['purposes'] as $purpose){ ?>
                                    <div class="col-lg-4 has-icon border-full red-bg pr-5 mb-5">
                                        <div class="icon mb-3 d-flex align-items-center justify-content-center">
                                            <img src="<?php echo $purpose['icon']; ?>">
                                        </div>
                                        <h6><?php echo $purpose['label']; ?></h6>
                                        <div class="paragraph t-opacity-75"><?php echo $purpose['text']; ?></div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }?>


            <?php if(isset($about['designs'] ) && sizeof( $about['designs'])>0 ) { ?>

            <div class="design py-5">
                <div class="section-title text-center mb-4"><?php echo $about['design_title']; ?></div>
                <div class="row">
                    <?php foreach ($about['designs'] as $design){ ?>
                    <div class="col-lg-3 mb-3">
                        <div class="design-div p-4 text-center">
                            <div class="icon mb-4">
                                <img src="<?php echo $design['icon']; ?>">
                            </div>
                            <h6><?php echo $design['label']; ?></h6>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <?php }?>

        </div>

    </div>


    <?php if(isset($about['features'] ) && sizeof( $about['features'])>0 ) { ?>
    <div class="features bg-image py-5" style="background-image: url('<?php echo $about['features_image']; ?>')">
        <div class="overlay d-flex justify-content-center align-items-center">
            <div class="container">
                <h4 class="font-weight-bold text-center mb-5"><?php echo $about['features_title']; ?></h4>
                <div class="row">
                    <?php foreach ($about['features'] as $feature){ ?>
                        <div class="col-lg-6 mb-3">
                            <div class="feature-div py-3 px-3 d-flex align-items-center"><i class="mr-2" data-feather="check-circle" width="12px"></i><span><?php echo $feature['label']; ?></span></div>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
    <?php }?>



    <?php if(isset($about['usages'] ) && sizeof( $about['usages'])>0 ) { ?>
    <div class="content">
        <div class="usage py-5">
            <h4 class="font-weight-bold mb-5"><?php echo $about['usages_title']; ?></h4>
            <div class="row">

                <?php $i=0; foreach ($about['usages'] as $usage){?>

                        <div class="<?php if($i%2==0){ ?> col-lg-7 <?php }else{ ?> col-lg-5 <?php } ?> ">
                            <div class="usage-div h-100">
                                <div class="py-4">
                                    <h6 class="m-0 text-center"><?php echo $usage['label']; ?></h6>
                                </div>
                                <?php if(isset($usage['list']) && sizeof($usage['list'])){ ?>
                                    <div class="bullet-pts px-lg-5 px-4 py-2">
                                        <?php foreach ($usage['list'] as $item){ ?>
                                            <div class="py-2 d-flex align-items-center"><i class="mr-2" data-feather="check-circle" width="12px"></i><span class="t-opacity-75"><?php echo $item['label']; ?></span></div>
                                        <?php  } ?>
                                    </div>
                                <?php }  ?>
                            </div>
                        </div>

                <?php $i=$i+1; }?>

            </div>
        </div>

    </div>
    <?php }?>



    <?php include('components/products.php'); ?>

</div>


<?php // $pages = get_pages([ "child_of" => get_the_ID(), "parent" => get_the_ID(), "sort_column" => "menu_order" ]); ?>
<!---->
<?php
//foreach ($pages AS $page){
//    $html = file_get_contents($page->guid); echo $html;
//} ?>





<?php get_footer(); ?>


