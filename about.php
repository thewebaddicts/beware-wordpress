<div class="menu-spacer"></div>
<?php /* Template Name: About */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>

<?php
$about = get_fields(get_the_ID());

global $post;
?>



<div class="about">

    <div class="page-banner" style="background-image: url('<?php echo $about['background_image']; ?>')">
        <div class="overlay"></div>
    </div>

    <div class="content">

        <div class="pb-5">

            <h3 class="font-weight-bold mb-3"><?php echo $post->post_title; ?></h3>

            <div class="breadcrumbs mb-5">
                <a href="/"><span>Homepage</span></a>
                <span class="mx-2">/</span>
                <a ><span><?php echo $post->post_title; ?></span></a>
            </div>

            <?php if(isset($about['paragraphs']) && sizeof($about['paragraphs'])>0 ){
                $i=0; foreach ($about['paragraphs'] as $paragraph){ ?>
                    <div class="row py-5">
                        <div class="col-lg-6 <?php if($i%2!=0){ ?> order-lg-1 <?php } ?>">
                            <div>
                                <img class="img" src="<?php echo $paragraph['image']; ?>">
                            </div>
                        </div>
                        <div class="col-lg-6 d-flex justify-content-center align-items-center <?php if($i%2!=0){ ?> order-lg-0 <?php } ?>" >
                            <div class="d-flex justify-content-center w-75">
                                <div>
                                    <h4 class="font-weight-bold mb-3"><?php if(isset($paragraph['label'])){ echo $paragraph['label'];} ?></h4>
                                    <div class="paragraph t-opacity-75 mb-4">
                                        <?php echo nl2br($paragraph['description']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php $i=$i+1; }  } ?>


            <div id="team"></div>
            <?php if(isset($about['members']) && sizeof($about['members'])){ ?>

                <div class="our-team py-5">
                    <h3 class="font-weight-bold mb-4"><?php echo $about['team_title']; ?></h3>
                    <div class="slider">
                        <div class="owl-carousel">
                            <?php foreach ($about['members'] as $member) { ?>
                                <div class="flip-card">
                                    <div class="flip-card-inner">
                                        <div class="flip-card-front text-center p-5">
                                            <img class="mb-4 mx-auto" src="<?php echo $member['image']; ?>">
                                            <h6 class="staff-name mb-3"><?php echo $member['name']; ?></h6>
                                            <div class="section-title"><?php echo $member['job']; ?></div>
                                        </div>
                                        <div class="flip-card-back text-center p-5">
                                            <h6 class="staff-name mb-3"><?php echo $member['name']; ?></h6>
                                            <div class="section-title mb-3"><?php echo $member['job']; ?></div>
                                            <div class="paragraph t-opacity-75">
                                                <?php echo $member['description']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>

                        </div>
                    </div>
                </div>

            <?php } ?>



        </div>

    </div>

</div>






<?php get_footer(); ?>


