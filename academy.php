<div class="menu-spacer"></div>
<?php /* Template Name: Academy */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>

<?php $page = get_fields(get_the_ID());?>
<?php global $post;  ?>



<div class="academy">

    <div class="page-banner" style="background-image: url('<?php echo $page['background_image'];  ?>');">
        <div class="overlay"></div>
    </div>

    <div class="content">
        <div class="pb-5">

            <h3 class="font-weight-bold mb-3"><?php echo $post->post_title; ?></h3>
            <div class="breadcrumbs mb-5">
                <a href="/"><span>Homepage</span></a>
                <span class="mx-2">/</span>
                <a><span><?php echo $post->post_title; ?></span></a>
            </div>

            <?php $home="no"; include('components/services.php'); ?>

        </div>
    </div>

    <?php if(isset($page['slides']) && sizeof($page['slides'])>0 ){ ?>
    <div class="slideshow mt-5">
        <div class="owl-carousel">
            <?php foreach ($page['slides'] as $slide){ ?>
            <div class="carousel-item bg-image" style="background-image: url('<?php echo $slide['image']; ?>');">
                <div class="overlay"></div>
            </div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
</div>




<?php get_footer(); ?>


