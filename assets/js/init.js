$(document).ready(function(){
    $('[data-youtube]').youtube_background();

    window.addEventListener('scroll', InitializeMenuScroll);
    function InitializeMenuScroll(){
        var scroll_start = 0;
        if (($(document).scrollTop() - 40) > 0){
            $('#main_menu').addClass('compact');
            // $('#main_menu .custom-logo').attr('src','/wp-content/themes/i4/assets/images/logo.svg');
            // $('header.main_menu nav a.toggle i').css('color','#000');
        }else{
            $('#main_menu').removeClass('compact');
            // $('#main_menu .custom-logo').attr('src','/wp-content/uploads/2021/02/i4-logo.png');
            // $('header.main_menu nav a.toggle i').css('color','#fff');
        }
    }



    $('.slideshow').find('.owl-carousel').owlCarousel({
        loop: true,
        items: 1,
        autoplay: false,
        autoplayTimeout: 5000
    });

    $('.slider').find('.owl-carousel').owlCarousel({
        // loop: true,
        autoWidth: true,
        dots: false,
        margin: 25
    });

    $('.small-slider').find('.owl-carousel').owlCarousel({
        loop: false,
        autoWidth: true,
        dots: false,
        margin: 10,
        nav:true
    });




    if($(".multi-carousel").length>0) {

        var owl2 = $(".multi-carousel")
        owl2.owlCarousel({
            //items:,
            autoWidth:true,
            autoplay: false,
            // rtl: false,
            loop: false,
            nav: false,
            dots:false,
            margin: 30,
            navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>']
            // animateOut: 'slideOutUp',
            // animateIn: 'slideInUp'
        });
        owl2.on('changed.owl.carousel', function(event) {
            bLazy.revalidate();
        });
        owl2.on('initialized.owl.carousel', function(event) {
            bLazy.revalidate();
        });
    }



    var rellax = new Rellax('.rellax', {
        speed: -2,
        center: false,
        wrapper: null,
        round: true,
        vertical: true,
        horizontal: false
    });






    $('.accordion a').click(function(){
        $(this).parent().toggleClass('expanded');
    });

    $('.modal').modal({
        onCloseStart : function(elem){ $(elem).find('iframe').attr( 'src', function ( i, val ) { return val; }); }
    });
    AOS.init({ easing: 'ease-in-out-sine', duration: 1000, once : true });

    $('ul.mobile .menu-item-has-children a').click(function(){
        $(this).parent().find('ul.sub-menu').first().toggleClass('expanded');
    });
    $('.main_menu .toggle').click(function(){
        $(this).toggleClass('expanded');
        $('ul.mobile').toggleClass('expanded');
    })


    $.fancyConfirm = function(opts) {
        opts = $.extend(
            true,
            {
                title: "Are you sure?",
                message: "",
                okButton: "OK",
                noButton: "Cancel",
                callback: $.noop
            },
            opts || {}
        );

        $.fancybox.open({
            type: "html",
            autoDimensions:true,
            src:
                '<div class="fc-content">' +
                "<h3>" +
                opts.title +
                "</h3>" +
                "<p>" +
                opts.message +
                "</p>" +
                '<div class="text-right confirmation">' +
                '<a data-value="0" data-fancybox-close>' +
                opts.noButton +
                "</a>" +
                '<button data-value="1" data-fancybox-close class="rounded">' +
                opts.okButton +
                "</button>" +
                "</div>" +
                "</div>",
            opts: {
                animationDuration: 350,
                animationEffect: "material",
                modal: true,
                baseTpl:
                    '<div class="fancybox-container fc-container" role="dialog" tabindex="-1">' +
                    '<div class="fancybox-bg"></div>' +
                    '<div class="fancybox-inner">' +
                    '<div class="fancybox-stage"></div>' +
                    "</div>" +
                    "</div>",
                afterClose: function(instance, current, e) {
                    var button = e ? e.target || e.currentTarget : null;
                    var value = button ? $(button).data("value") : 0;

                    opts.callback(value);
                }
            }
        });
    };


    AccordionInitiate();
    FooterFunction();

});
feather.replace();



function carouselPrev(elem){
    // $(elem).hide();
    var elems = $(elem).parents('.carousel-container').find('.carousel')
    var moveRight = M.Carousel.getInstance(elems);
    moveRight.prev(1);
}

function carouselNext(elem){
    // $(elem).hide();
    var elems = $(elem).parents('.carousel-container').find('.carousel')
    var moveRight = M.Carousel.getInstance(elems);
    moveRight.next(1);
}

function changeQueryParameter(key , value ,location){
    var uri = window.location.href;
    var location_str = location ? "#"+location : "";
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf("?") !== -1 ? "&" : "?";
    if (uri.match(re)) {
        if( uri.indexOf('#') !== -1 ){
            uri = uri.replace(/#.*/, '');
        }
        window.location.href = uri.replace(re, "$1" + key + "=" + value + "$2") + location_str;
    } else {
        if( uri.indexOf('#') !== -1 ){
            uri = uri.replace(/#.*/, '');
        }
        window.location.href = uri + separator + key + "=" + value + location_str;
    }
}

function  searchArticle() {
    var value = $('.search-input').val().toLowerCase();

    $(".articles-row .article-card-container").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}

function FillApplication(position){
    $('#apply_position').val(position);
    $('html, body').animate({
        scrollTop: $("#applyjob_form").offset().top - 100
    },'slow');
}

function scrollToAName(scrollto){
    $('html, body').animate({
        scrollTop: $('#'+scrollto).offset().top - 100
    }, 500);
}

function AccordionInitiate(){
    $('.accordion .accordion-item:not(.active) .accordion-content').addClass('initialized');
    $('.accordion .accordion-item:not(.active) .accordion-content').slideUp(0);
    $('.accordion .accordion-title').click(function(){
        $(this).parents('.accordion').find('.active').removeClass('active');
        $(this).parents('.accordion-item').toggleClass('active');
        $(this).parents('.accordion-item').find('.accordion-content').slideToggle(200);

        try {
            bLazy.revalidate();
        }catch (e) {

        }
        $(this).parents('.accordion').find('.accordion-item:not(.active)').find('.accordion-content').slideUp(200);
    });

}