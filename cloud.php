<div class="menu-spacer"></div>
<?php /* Template Name: Cloud */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>

<?php $page = get_fields(get_the_ID()); ?>
<?php global $post;  ?>

<div class="cloud-page">

    <div class="page-banner" style="background-image: url('<?php echo $page['background_image']; ?>');">
        <div class="overlay"></div>
    </div>

    <div class="content">

        <div class="pb-5">

            <h3 class="font-weight-bold mb-3"><?php echo $post->post_title; ?></h3>

            <div class="breadcrumbs mb-5">
                <a href="/"><span>Homepage</span></a>
                <span class="mx-2">/</span>
                <a><span><?php echo $post->post_title; ?></span></a>
            </div>

            <div class="paragraph t-opacity-75 py-5">
                <?php echo nl2br($page['introduction']); ?>
            </div>

            <div class="cloud py-5">
                <div class="row">
                    <div class="col-lg-6">
                        <h4 class="font-weight-bold mb-3"> <?php echo $page['cloud_section_title']; ?></h4>
                        <div class="paragraph t-opacity-75 mb-5"><?php echo nl2br($page['cloud_section_text']); ?></div>
                    </div>
                </div>

                <?php include('components/cloud-list.php') ?>

            </div>


            <?php if(isset($page['courses']) && sizeof($page['courses'])>0){ ?>
                <div class="row">
                    <div class="col-lg-6">
                        <?php if(isset($page['courses_title'])){ ?><h4 class="font-weight-bold mb-3"> <?php echo $page['courses_title']; ?></h4><?php } ?>
                        <?php if(isset($page['courses_text'])){ ?><div class="paragraph t-opacity-75 mb-4"><?php echo nl2br($page['courses_text']); ?></div><?php } ?>
                    </div>
                </div>

                <div class="row my-4">
                <?php foreach ($page['courses'] as $course){ ?>
                    <div class="col-lg-3 col-sm-6 mb-lg-0 mb-4">
                        <div class="course-card">
                            <img src="<?php echo $course['image']; ?>">
                            <h6 class="title py-2"><?php echo $course['label']; ?></h6>
                            <div class="description"><?php echo nl2br($course['description']); ?></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?php } ?>


        </div>

    </div>

</div>






<?php get_footer(); ?>


