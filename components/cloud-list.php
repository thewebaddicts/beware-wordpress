<?php

if (pll_current_language() == "fr")
    try {
        $page = get_fields('1101');
    }catch(Throwable $th){ echo "page not found";}
else
    $page = get_fields('50');


?>
<div class="cloud">

<?php if(isset($page['cloud']) && sizeof($page['cloud'])>0 ){
    $size = sizeof($page['cloud']);
    $nb=$size/4;
    if((int) $nb < $nb ){
        $nb_rows=(int) $nb +1;
    }else{
        $nb_rows=$size/4;
    }

    $i=0;
    ?>

    <div class="list-container">
        <?php $rows=1;
        while($rows<=$nb_rows){
        ?>
        <div class="row <?php if($rows!=1){ ?> pt-cloud-desktop <?php } ?>">
            <?php if($rows < $nb_rows){ ?> <div class="has-border-right"></div> <?php } ?>
            <?php if($rows<$nb_rows){ ?> <div class="has-border-bottom"></div> <?php } ?>

            <?php  while($i<$rows*4 && $i<sizeof($page['cloud'])){  ?>
                <div class="col-lg-3 has-icon <?php if($i==sizeof($page['cloud'])-1){ ?> no-before <?php } ?> pr-5 mb-5">
                    <?php if($rows!=1){ if($i==4*$rows-4){ ?> <div class="cloud-border-top"></div> <?php  } } ?>
                    <div class="icon mb-3 d-flex align-items-center justify-content-center">
                        <img src="<?php echo $page['cloud'][$i]['icon']; ?>">
                    </div>
                    <h6><?php echo $page['cloud'][$i]['label']; ?></h6>
                    <div class="paragraph"><?php echo nl2br($page['cloud'][$i]['description']); ?></div>
                </div>
            <?php $i=$i+1;  } ?>

<!--            --><?php //if($rows==$nb_rows){ ?>
<!--            <div class="col-lg-3 pr-5 mb-5 btn-div">-->
<!--                <div class="d-flex h-100 align-items-center">-->
<!--                    <a href="--><?php //echo get_permalink(20); ?><!--" class="big-btn">Request Demo<i class="fas fa-arrow-right"></i></a>-->
<!--                </div>-->
<!--            </div>-->
<!--            --><?php //} ?>

        </div>
        <?php $rows++; } ?>
    </div>
<?php } ?>


</div>

<script language="javascript">
    function FooterFunction() {

        var icon_after = $('.has-icon');
        var icon = $('.has-icon .icon');
        var heightThreshold = $(".cloud").offset().top - 500;
        var heightThreshold_end = $(".cloud").offset().top + $(".cloud").height();
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();

            if (scroll >= heightThreshold && scroll <= heightThreshold_end) {


                var delay = 0;
                var counter=0;
                $.each( icon_after, function(){
                    var element = $(this);
                    setTimeout(function(){
                        element.find('.icon').addClass('active');
                        element.addClass('active');

                    },delay);
                    counter++;

                    delay += 400;
                    if(counter===4){
                        setTimeout(function(){
                            $('.cloud .row .has-border-right').first().addClass('active');
                        },delay);
                        delay += 400;
                        setTimeout(function(){
                            $('.cloud .row .has-border-bottom').first().addClass('active');
                        },delay);
                        delay += 400;
                        setTimeout(function(){
                            $('.cloud .row .cloud-border-top').first().addClass('active');
                        },delay);
                        delay += 400;
                    }
                    if(counter===8){
                        setTimeout(function(){
                            $('.cloud .row .has-border-right').addClass('active');
                        },delay);
                        delay += 400;
                        setTimeout(function(){
                            $('.cloud .row .has-border-bottom').addClass('active');
                        },delay);
                        delay += 400;
                        setTimeout(function(){
                            $('.cloud .row .cloud-border-top').addClass('active');
                        },delay);
                        delay += 400;
                    }
                });

                // icon_after.addClass('active');
                // $('.cloud .row').addClass('active-after');
                // $('.cloud .row').addClass('active-before');
                // icon.addClass('active');
            } else {
                icon_after.removeClass('active');
                $('.cloud .row .has-border-right').removeClass('active');
                $('.cloud .row .has-border-bottom').removeClass('active');
                $('.cloud .row .cloud-border-top').removeClass('active');
                icon.removeClass('active');
            }
        });

    }
</script>
