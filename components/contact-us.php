<?php

if (pll_current_language() == "fr")
    $page = get_fields('1097');
else
    $page = get_fields('20');

$countries=['Afghanistan','Albania','Algeria','Andorra','Angola','Antigua&Deps','Argentina','Armenia','Australia','Austria','Azerbajian','Bahamas','Bahrain','Bangladesh','Barbados','Belarus','Belgium','Belize','Benin','Bhutan','Bolivia','Bosnia Herzegovina','Botswana','Brazil','Brunei','Bulgaria','Burkina','Burundi','Cambodia','Cameron','Canada','Cape Verde','Central African Rep','Chad,','Chile','China','Colombia','Comoros','Congo','Congo {Democratic Rep}','Costa Rica','Croatia','Cuba','Cyprus','Czech Republic','Denmark','Djibouti','Dominica','Dominican Republic','East Timor','Ecuador','Egypt','El Salvador','Equatorial Guinea','Eritrea','Estonia','Ethiopia','Fiji','Finland','France','Gabon','Gambia','Georgia','Germany','Ghana','Greece','Grenada','Guatemala','Guinea','Guinea-Bissau','Guyana','Haiti','Honduras','Hungary','Iceland','India','Indonesia','Iran','Iraq','Ireland {Republic}','Israel','Italy','Ivory Coast','Jamaica','Japan','Jordan','Kazakhstan','Kenya','Kiribati','Korea North','Korea South','Kosovo','Kuwait','Kyrgyzstan','Laos','Latvia','Lebanon','Lesotho','Liberia','Libya','Liechtenstein','Lithuania','Luxembourg','Macedonia','Madagscar','Malawi','Malaysia','Maldives','Mali','Malta','Marshall Islands','Mauritania','Mauritius','Mexico','Micronesia','Moldova','Monaco','Mongolia','Montenegro','Morocco','Mozambique','Myanmar, {Burma}','Namibia','Nauru','Nepal','Netherlands','New Zealand','Nicaragua','Niger','Nigeria','Norway','Oman','Pakistan','Palau','Panama','Papua New Guinea','Paraguay','Peru','Philippines','Poland','Portugal','Qatar','Romania','Russian Federation','Rwanda','St Kitts & Nevis','St Lucia','Saint Vincent & the Grenadines','Samoa','San Marino','Sao Tome & Principe','Saudi Arabia','Senegal','Serbia','Seychelles','Sierra Leone','Singapore','Slovakia','Slovenia','Solomon Islands','Somalia','South Africa','South Sudan','Spain','Sri Lanka','Sudan','Suriname','Swaziland','Sweden','Switzerland','Syria','Taiwan','Tajikistan','Tanzania','Thailand','Togo','Tonga','Trinidad & Tobago','Tunisia','Turkey','Turkmenistan','Tuvalu','Uganda','Ukraine','United Arab Emirates','United Kingdom','United States','Uruguay','Uzbekistan','Vanuatu','Vatican City','Venezuela','Vietnam','Yemen','Zambia','Zimbabwe'];
 ?>
<div class="content py-5">
    <div class="contact-form d-flex justify-content-center py-5">
        <form action="#" method="POST">
            <div class="w-50 m-auto text-center">
                <div class="section-title mb-2"><?php echo $page['form_label']; ?></div>
                <h4 class="font-weight-bold mb-4"><?php echo $page['form_title']; ?></h4>
            </div>
            <div class="row mb-3">
                <div class="col-lg-6 mb-3">
                    <div class="input-div">
                        <div class="form-label"><?php echo $page['first_name']['label']; ?></div>
                        <input class="form-input" type="text" name="first_name" placeholder="<?php echo $page['first_name']['placeholder']; ?>" required>
                    </div>
                </div>
                <div class="col-lg-6 mb-3">
                    <div class="input-div">
                        <div class="form-label"><?php echo $page['last_name']['label']; ?></div>
                        <input class="form-input" type="text" name="last_name" placeholder="<?php echo $page['last_name']['placeholder']; ?>" required>
                    </div>
                </div>
                <div class="col-lg-6 mb-3">
                    <div class="input-div">
                        <div class="form-label"><?php echo $page['email_address']['label']; ?></div>
                        <input class="form-input" type="email" name="email" placeholder="<?php echo $page['email_address']['placeholder']; ?>" required>
                    </div>
                </div>
                <div class="col-lg-6 mb-3">
                    <div class="input-div">
                        <div class="form-label"><?php echo $page['phone']['label']; ?></div>
                        <input class="form-input" type="tel" name="phone" placeholder="<?php echo $page['phone']['placeholder']; ?>">
                    </div>
                </div>
                <div class="col-lg-6 mb-3">
                    <div class="input-div">
                        <div class="form-label"><?php echo $page['organization']['label']; ?></div>
                        <input class="form-input" type="text" name="organization" placeholder="<?php echo $page['organization']['placeholder']; ?>" required>
                    </div>
                </div>
                <div class="col-lg-6 mb-3">
                    <div class="input-div">
                        <div class="form-label"><?php echo $page['position']['label']; ?></div>
                        <input class="form-input" type="text" name="position" placeholder="<?php echo $page['position']['placeholder']; ?>" required>
                    </div>
                </div>
                <div class="col-lg-12 mb-3">
                    <div class="input-div">
                        <div class="form-label"><?php echo $page['country']['label']; ?></div>
                        <select class="form-input d-block t-opacity-50" name="country">
                            <option value="" class="" disabled selected><?php echo $page['country']['placeholder']; ?></option>
                             <?php foreach ($countries as $country) {?>
                                 <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                             <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12 mb-3">
                    <div class="input-div">
                        <div class="form-label"><?php echo $page['message']['label']; ?></div>
                        <textarea class="form-input" type="text" name="message" placeholder="<?php echo $page['message']['placeholder']; ?>" rows="3" required></textarea>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <button type="submit" name="submit" class="big-btn"><?php echo $page['button_label']; ?><i class="fas fa-arrow-right"></i></button>
            </div>

            <?php

            //                $my_custom_filename = time() . $_FILES['attachment']['name'];
            //                wp_upload_bits($my_custom_filename , null, file_get_contents($_FILES['fileToUpload']['tmp_name']));

            $postData = $uploadedFile = $statusMsg = '';
            $msgClass = 'errordiv';
            if(isset($_POST['submit'])){
                // Get the submitted form data
                $postData = $_POST;
                $email = $_POST['email'];
                $name = $_POST['first_name'].' '.$_POST['last_name'];
                $phone= $_POST['phone'];
                $organization= $_POST['organization'];
                $position= $_POST['position'];
                if(isset($_POST['country'])){
                    $country= $_POST['country'];
                }else{
                    $country= "-";
                }

                $subject = 'Contact us Form Submit';
                $message = $_POST['message'];

                // Check whether submitted data is not empty
                if(!empty($email) && !empty($name) && !empty($subject) && !empty($message)){

                    // Validate email
                    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
                        echo 'Please enter your valid email.';
                    }else{

                        // Recipient
                        $toEmail = trim($page['send_form_to_email']);

                        // Subject
                        $emailSubject = 'Contact us Form Submit';


                        // Message
                        $htmlContent = '<h2>'.$emailSubject.'</h2>
                    <p><b style="color:#00A48E">Full Name:</b> '.$name.'</p>
                    <p><b style="color:#00A48E">Email:</b> '.$email.'</p>
                    <p><b style="color:#00A48E">Phone:</b> '.$phone.'</p>
                    <p><b style="color:#00A48E">Organization:</b> '.$organization.'</p>
                    <p><b style="color:#00A48E">Position:</b> '.$position.'</p>
                    <p><b style="color:#00A48E">Country:</b> '.$country.'</p>
                    <p><b style="color:#00A48E">Message:</b><br/>'.$message.'</p>';



                        // Send email
                        sendmail($emailSubject, $toEmail, $htmlContent);


                    }
                }else{
                    $statusMsg = 'Please fill all the fields.';
                }
            }



            ?>

        </form>
    </div>
</div>