<?php


if (pll_current_language() == "fr")
    $events = get_fields('1119');
else
    $events = get_fields('47');

?>
<div class="events-slider">

    <div class="slider">
        <div class="owl-carousel">
            <?php if(isset($events) && sizeof($events)>0){
                foreach($events['events_services'] as $event){ ?>

                    <div class="event-card d-flex justify-content-center align-items-center">
                        <div>
                            <div class="icon mb-4 d-flex justify-content-center">
                                <div class="d-flex justify-content-center align-items-center">
                                    <img class="contain" src="<?php echo $event['icon']; ?>">
                                </div>
                            </div>
                            <h6 class="mb-0 title text-center px-2"><?php echo $event['label']; ?></h6>
                        </div>
                    </div>


                <?php } } ?>

        </div>
    </div>
</div>
