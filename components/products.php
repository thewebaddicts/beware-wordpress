<?php
$products = get_pages( array(
    'child_of'    => '46',
    'sort_column'=> 'menu_order'
));

if (pll_current_language() == "fr")
    $section = get_fields('1123');
else
    $section = get_fields('2');

?>
<div class="our-products py-5">
    <div class="content">
        <div class="section-title"><?php echo $section['products_label']; ?></div>
        <h4 class="section-main-title my-3"><?php echo $section['products_title']; ?></h4>
    </div>

    <div class="slider mt-4">
        <div class="owl-carousel">
            <?php foreach ($products as $product){
                $info = get_fields($product->ID);
                ?>
                <div class="product-card bg-image" style="background-image: url('<?php echo $info['products_details']['image']; ?>');">
                    <div class="overlay d-flex align-items-end d-flex align-items-end">
                        <div class="product-info">
                            <div class="icon" style="background-image: url('<?php echo $info['products_details']['logo']; ?>')">
                            </div>
                            <div class="product-desc">
                                <div class="my-3 t-opacity-70"><?php echo $info['products_details']['small_description']; ?></div>
                                <a href="<?php echo get_permalink($product->ID); ?>" class="learn-more"><?php if (pll_current_language() == "fr"){ echo 'APPRENDRE PLUS'; }else{ echo 'LEARN MORE'; } ?><i class="fas fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>