<?php
if (pll_current_language() == "fr")
    $page = get_fields('49');
else
    $page = get_fields('49');

?>


<div class="row mt-5">
    <?php $i=0; if(isset($page['services']) && sizeof($page['services'])>0 ) {
        foreach ($page['services'] as $service){
            if($home=="yes"){
                if($i<3){
                    ?>
                    <div class="col-lg-4 mb-lg-4 mb-3">
                        <div class="service-div p-4 h-100">
                            <div class="icon mb-3">
                                <img src="<?php echo $service['icon']; ?>">
                            </div>
                            <h6 class="mb-3"><?php echo $service['label']; ?></h6>
                            <div class="paragraph t-opacity-75"><?php echo nl2br($service['description']); ?></div>
                        </div>
                    </div>
                <?php } $i++;  }else{  ?>

                <div class="col-lg-4 mb-lg-4 mb-3">
                    <div class="service-div p-4 h-100">
                        <div class="icon mb-3">
                            <img src="<?php echo $service['icon']; ?>">
                        </div>
                        <h6 class="mb-3"><?php echo $service['label']; ?></h6>
                        <div class="paragraph t-opacity-75"><?php echo nl2br($service['description']); ?></div>
                    </div>
                </div>

            <?php } ?>

        <?php } ?>



    <?php } ?>

</div>

