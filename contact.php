<div class="menu-spacer"></div>
<?php /* Template Name: Contact us */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>


<?php $page = get_fields(get_the_ID());?>
<?php global $post;  ?>



<div class="contact-us">

    <div class="page-banner" style="background-image: url('<?php echo $page['background_image']; ?>');">
        <div class="overlay"></div>
    </div>

    <div class="content">

        <div class="pb-5">

            <h3 class="font-weight-bold mb-3"><?php echo $post->post_title; ?></h3>

            <div class="breadcrumbs mb-5">
                <a href="/"><span>Homepage</span></a>
                <span class="mx-2">/</span>
                <a><span><?php echo $post->post_title; ?></span></a>
            </div>

            <div class="row py-5">
                <div class="col-lg-5 accordion">
                    <?php $i=0; foreach ($page['branches'] as $branch ){  $i++; ?>
                    <div class="address-div accordion-item mb-4" onclick="new google.maps.event.trigger(MarkersArrayElems['<?php echo $i; ?>'], 'click' )" >
                        <div class="accordion-title title-div p-4">
                            <h6 class="font-weight-bold"><?php echo $branch['name']; ?></h6>
                            <div class="mb-3 d-flex align-items-center"><i data-feather="map-pin" width="16px" class="mr-2"></i><span class="paragraph"><?php echo $branch['country']; ?></span></div>
                        </div>

                        <div class="address-details p-4 accordion-content">
                            <div class="address mb-2"><?php echo $branch['address']; ?></div>
                            <div class="email mb-2"><?php echo $branch['email']; ?></div>
                            <div class="phone mb-2"><?php echo $branch['phone']; ?></div>
                            <a href="javascript:;" class="learn-more">Get Directions<i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                    <?php } ?>
                </div>

                <div class="col-lg-7">
                    <div class="mapContent" id="map" data-aos="fade-up" style="height: 100%;min-height:500px;"></div>
<!--                    <iframe class="map w-100 h-100" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26487.486493389217!2d35.571171296662236!3d33.91705156113012!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151f3e6b447ec40d%3A0x6429a038db5c652c!2sAntelias!5e0!3m2!1sen!2slb!4v1622893747311!5m2!1sen!2slb" style="border:0;" allowfullscreen="" loading="lazy"></iframe>-->
                </div>
            </div>

        </div>

    </div>

</div>



<?php
include('components/contact-us.php')
?>







<?php get_footer(); ?>

<script language="javascript">
    var MarkersArrayElems = [];
    function initMap() {
        var settings = {
            center: new google.maps.LatLng(44, -110),
            zoom: 12
        };
        //console.log(settings);
        var map = new google.maps.Map(document.getElementById('map'), settings);
        var bounds = new google.maps.LatLngBounds();
        //   console.log(bounds);
        var infowindow = new google.maps.InfoWindow({ maxWidth: 300 });
        var MarkersArray = [];
        <?php $i=0;
        foreach ($page['branches'] as $branch) {
            $i++;
        $var = "Country: " .$branch['country'];




        ?>
        MarkersArray.push(['<?php echo $branch['latitude'] ?>','<?php echo $branch['longitude'] ?>','<?php echo $var ?>','<?php echo $branch['name']; ?>' ,'<?php echo $i ?>']);
        <?php } ?>
        for (var i = 0; i < MarkersArray.length; i++) {
            var single_location = MarkersArray[i];
            var myLatLng = new google.maps.LatLng(single_location[0], single_location[1]);
            var marker = new google.maps.Marker({
                position: myLatLng,
                latlng : single_location[0]+','+single_location[1],
                map: map,
                title: single_location[3],
                description: single_location[2],
                gridid : single_location[4],
            });
            MarkersArrayElems[single_location[4]]=marker;
            bounds.extend(marker.getPosition());
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent('<div class="ContactMapTooltip"><h6 class=\'t-3 mt-0 mb-2 \'>' + this.title + '</h6><span class=\'t-1 mt-0 mb-1 d-block\'>'+this.description+'</span><br><a class="BackgroundGradient" target="_blank" href="https://www.google.com/maps/search/?api=1&query='+ this.latlng +'">View in Google Maps</a></div>');
                infowindow.open(map, this);
            });
        }
        map.fitBounds(bounds);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBP1RJInQYbN9VPbKFs_1IJUluULU_TZrQ&callback=initMap"
        async defer></script>
