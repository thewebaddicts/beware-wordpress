<?php /* Template Name: Main Template for Products */ ?>
<?php get_header(); ?>
<?php $page = get_fields(get_the_ID());?>
<?php global $post;  ?>

<?php
// display nav only if there are child pages
$products = get_pages( array(
    'child_of'    => '46',
    'sort_column'=> 'menu_order'
));
$pages = array();

foreach ($products as $product) {
    $pages[] += $product->ID;
}
$current = array_search($post->ID, $pages);
$prevID = $pages[$current - 1];
$nextID = $pages[$current + 1];

?>

<div class="menu-spacer"></div>


<div class="product-details">

    <div class="page-banner" style="background-image: url('<?php echo $page['products_details']['image']; ?>');">
        <div class="overlay"></div>
    </div>

    <div class="content">

        <div class="pb-5">

            <img class="mb-3 contain logo" src="<?php echo $page['products_details']['logo']; ?>">

            <div class="breadcrumbs mb-5 mt-4">
                <a href="/"><span>Homepage</span></a>
                <span class="mx-2">/</span>
                <a ><span>Products</span></a>
                <span class="mx-2">/</span>
                <a ><span><?php echo $page['products_details']['label']; ?></span></a>
            </div>

            <div class="pt-5">
                <h4 class="font-weight-bold mb-3"><?php echo $page['products_details']['introduction']['label']; ?></h4>
                <div class="paragraph mb-4 t-opacity-75">
                    <?php echo nl2br($page['products_details']['introduction']['text']); ?>
                </div>
            </div>

            <div class="product-info py-5">
                <?php try{ if(isset($page['products_details']['banners']) && sizeof($page['products_details']['banners'])>0 ){
                    foreach ($page['products_details']['banners'] as $banner){
                    ?>
                <div class="bg-image my-5" style="background-image: url('<?php echo $banner['image']; ?>');">
                    <div class="row">
                        <div class="col-lg-6 <?php if(isset($banner['text_direction']) && $banner['text_direction']=="right"){ ?> order-1 <?php } ?>">
                            <div class="d-flex align-items-center h-100">
                                <div class="p-lg-5 p-4">
                                    <h4 class="font-weight-bold mb-3"><?php echo $banner['label']; ?></h4>
                                    <div class="paragraph t-opacity-75"><?php echo nl2br($banner['text']); ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 <?php if(isset($banner['text_direction']) && $banner['text_direction']=="right"){ ?> order-0 <?php } ?>"></div>
                    </div>
                </div>
                <?php } }  }catch(Throwable $e){} ?>

            </div>

            <div class="" style="overflow-x: auto">
                <?php echo nl2br($page['products_details']['big_description']); ?>
            </div>

            <div class="d-flex justify-content-between mt-3">
                <?php if (!empty($prevID)) { /* if we are browsing first child, then link to parent page*/ ?>
                    <a rel="prev" class="big-btn" href="<?php echo get_permalink($prevID); ?>" title="<?php echo get_the_title($prevID); ?>"><span class="meta-nav">&larr;</span> <?php echo get_the_title($prevID); ?></a>
                <?php }

                if (!empty($nextID)) { ?>
                    <a rel="next" class="big-btn" href="<?php echo get_permalink($nextID); ?>" title="<?php echo get_the_title($nextID); ?>"><?php echo get_the_title($nextID); ?> <span class="meta-nav">&rarr;</span></a>
                <?php } ?>
            </div>

        </div>

    </div>

</div>





<?php get_footer(); ?>

