<div class="menu-spacer"></div>
<?php /* Template Name: Events */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>

<?php //$page = get_fields('47');  ?>
<?php $page = get_fields(get_the_ID());?>

<?php global $post;  ?>



<div class="events">

    <div class="page-banner" style="background-image: url('<?php echo $page['background_image']; ?>');">
        <div class="overlay"></div>
    </div>

    <div class="content">

        <div class="pb-5">

            <h3 class="font-weight-bold mb-3"><?php echo $post->post_title; ?></h3>

            <div class="breadcrumbs mb-5">
                <a href="/"><span>Homepage</span></a>
                <span class="mx-2">/</span>
                <a href="events.php"><span><?php echo $post->post_title; ?></span></a>
            </div>

            <div class="paragraph t-opacity-75"><?php echo nl2br($page['events_intro']); ?></div>

        </div>

    </div>

    <?php include('components/events.php'); ?>

    <?php if(isset($page['events_actual']) && sizeof($page['events_actual'])>0 ){ ?>
    <div class="content mt-5">

        <div class="py-5">
            <h5 class="font-weight-bold text-center w-50 m-auto"><?php echo $page['events_actual']['label']; ?></h5>
            <div class="events-container row mt-5" style="margin-right:0;margin-left:0;">
                <?php if(isset($page['events_actual']['events']) && sizeof($page['events_actual']['events'])>0 ){ $i=0;
                    foreach ($page['events_actual']['events'] as $event){ ?>
                        <div class="col-lg-6 <?php if($i<sizeof($page['events_actual']['events'])-1){ ?> border-bottom <?php } ?> py-4 px-0 d-flex">
                            <div class="calendar d-flex justify-content-center align-items-center">
                                <div class="text-center">
                                    <i class="mb-2" data-feather="calendar" stroke-width="1"></i>
                                    <h6 class="font-weight-bold text-center"><?php echo $event['date']; ?></h6>
                                    <?php if(isset($event['date_to']) && $event['date_to']!=""){ ?><h6 class="font-weight-bold text-center">- <?php echo $event['date_to']; ?></h6><?php } ?>
                                    <div class="paragraph text-center t-opacity-50"><?php echo $event['time']; ?></div>
                                </div>
                            </div>

                            <div class="event-details px-4">
                                <h6><?php echo $event['label']; ?></h6>
                                <div class="paragraph mb-3 t-opacity-75"><?php echo $event['text']; ?></div>
                                <?php if(isset($event['images']) && sizeof($event['images'])>0 ){?>
                                    <div class="small-slider">
                                        <div class="owl-carousel">
                                            <?php foreach ($event['images'] as $image) { ?>
                                                <a href="<?php echo $image; ?>" data-fancybox="event-gallery" class="event-gallery">
                                                    <img src="<?php echo $image; ?>">
                                                </a>

                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php $i++; } } ?>
            </div>
        </div>

    </div>
    <?php } ?>
</div>









<?php get_footer(); ?>


