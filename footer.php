
<?php


if (pll_current_language() == "fr")
    $info = get_fields('1123');
else
    $info = get_fields('2');

?>
<div class="footer">

    <div class="footer-menu">

        <div class="content">
            <div class="row pt-5 pb-4">
                <div class="col-lg-6 d-flex">
                    <div class="flex-grow-1 mb-4">
                        <h6 class="font-weight-bold mb-4"><?php if (pll_current_language() == "fr"){ echo 'Plan du Site'; }else{ echo 'Sitemap'; } ?></h6>
                        <div class="paragraph">
                            <?php wp_nav_menu([
                                "menu" => "footer",
                                "theme_location" => "footer",
                                "container" => "",
                                "items_wrao" => "<ul></ul>",
                                "depth" => 1,
                            ]); ?>
                        </div>
                    </div>
                    <div class="flex-grow-1 mb-4">
                        <h6 class="font-weight-bold mb-4"><?php if (pll_current_language() == "fr"){ echo 'Nos Produits'; }else{ echo 'Our Products'; } ?></h6>
                        <div class="paragraph">

                        <?php  $pages = wp_list_pages( array('child_of' => '46', 'title_li' => '') );?>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 d-flex">
                    <div class="flex-grow-1 mb-4">
                        <h6 class="font-weight-bold mb-4"><?php if (pll_current_language() == "fr"){ echo 'Liens Rapides'; }else{ echo 'Quick Links'; } ?></h6>
                        <div class="paragraph">
                            <?php wp_nav_menu([
                                "menu" => "footer-2",
                                "theme_location" => "footer-2",
                                "container" => "",
                                "items_wrao" => "<ul></ul>",
                                "depth" => 1,
                            ]); ?>
                        </div>
                    </div>
                    <div class="flex-grow-1 mb-4">
                        <h6 class="font-weight-bold mb-4">Follow Us</h6>
                        <div class="d-flex">
                            <?php if(isset($info['facebook']) && $info['facebook']!=""){ ?><a href="<?php echo $info['facebook']; ?>" target="_blank" class="social-media d-flex justify-content-center align-items-center mr-3"><i class="fab fa-facebook-f"></i></a><?php } ?>
                            <?php if(isset($info['twitter']) && $info['twitter']!=""){ ?><a href="<?php echo $info['twitter']; ?>" target="_blank" class="social-media d-flex justify-content-center align-items-center mr-3"><i class="fab fa-twitter"></i></a><?php } ?>
                            <?php if(isset($info['linked_in']) && $info['linked_in']!=""){ ?><a href="<?php echo $info['linked_in']; ?>" target="_blank" class="social-media d-flex justify-content-center align-items-center mr-3"><i class="fab fa-linkedin-in"></i></a><?php } ?>
                            <?php if(isset($info['instagram']) && $info['instagram']!=""){ ?><a href="<?php echo $info['instagram']; ?>" target="_blank" class="social-media d-flex justify-content-center align-items-center"><i class="fab fa-instagram"></i></a><?php } ?>
                            <?php if(isset($info['youtube']) && $info['youtube']!=""){ ?><a href="<?php echo $info['youtube']; ?>" target="_blank" class="social-media d-flex justify-content-center align-items-center"><i class="fab fa-youtube"></i></a><?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center paragraph py-4">
                <div class="">Copyright © <?php echo date("Y");  ?> Beware Cyberlabs. All rights reserved</div>
                <div class="">Designed & developed by <a href="https://thewebaddicts.com/" target="_blank" class="font-weight-bold d-inline-block">The Web Addicts</a></div>

            </div>
        </div>



    </div>



</div>
<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/youtube-bg/jquery.youtube-background.js'; ?><!--"></script>-->
<!--<script language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.js"></script>-->
<?php
wp_footer();
?>
<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/aos/aos.js'; ?><!--"></script>-->
<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/parallax-js/parallax.min.js'; ?><!--"></script>-->
<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/init.js'; ?><!--"></script>-->
</body>
</html>
