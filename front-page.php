<?php get_header(); ?>
<?php
//$Banner = get_fields('2');
$post_id = pll_get_post( get_the_ID(), pll_current_language() );
$Banner =get_fields($post_id);

?>

<?php

if (pll_current_language() == "fr")
    $news = get_pages([ "child_of" => '1215']);
else
    $news = get_pages([ "child_of" => '84']);


$c=0;
foreach ($news as $new){
    if(isset($new->display_homepage) && $new->display_homepage=="yes"){
        $c++;
    }
}

?>

<div class="menu-spacer"></div>


<div class="homepage">


    <div class="slideshow">
        <div class="owl-carousel">
            <?php foreach( $Banner['slide'] AS $slide){ ?>
            <div class="carousel-item bg-image" style="background-image: url('<?php echo $slide['image']; ?>');">
                <div class="overlay">
                    <div class="content">
                        <div class="d-flex align-items-center">
                            <div class="slideshow-text">
                                <h1 class="mb-lg-4 mb-3 title"><?php echo $slide['label']; ?></h1>
                                <div class="t-opacity-75 mb-lg-4 mb-3"><?php echo $slide['text']; ?></div>
                                <?php if(isset($slide['button_label']) && $slide['button_label']!=""){ ?> <a href="<?php echo $slide['button_link'] ?>" class="big-btn d-inline-block"><?php echo $slide['button_label'] ?><i class="fas fa-arrow-right"></i></a> <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>


    <?php include('components/products.php'); ?>


<!--    about-->
    <?php if(!isset($Banner['about_display']) || $Banner['about_display']=="yes"){ ?>
    <div class="content">
        <div class="about-us my-5">
            <div class="row">
                <div class="col-lg-6">
                    <div>
                        <img class="img" src="<?php echo $Banner['about_image']; ?>">
                    </div>
                </div>
                <div class="col-lg-6 mt-lg-0 mt-3 d-flex justify-content-center align-items-center">
                    <div class="d-flex justify-content-center w-75">
                        <div>
                            <div class="section-title mb-2"><?php echo $Banner['about_label']; ?></div>
                            <h4 class="font-weight-bold mb-3"><?php echo $Banner['about_title']; ?></h4>
                            <div class="paragraph t-opacity-75 mb-4">
                                <?php echo nl2br($Banner['about_description']); ?>
                            </div>
                            <a href="<?php echo $Banner['page_link']; ?>" class="d-inline-block learn-more"><?php echo $Banner['about_button_label'] ?><i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>


<!--Events-->
    <?php if(!isset($Banner['events_display']) || $Banner['events_display']=="yes"){ ?>
    <div class="events my-5">
        <div class="content">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title mb-2"><?php echo $Banner['events_label']; ?></div>
                    <h4 class="font-weight-bold mb-3"><?php echo $Banner['events_title']; ?></h4>
                    <div class="paragraph mb-4 t-opacity-75"><?php echo nl2br($Banner['events_text']); ?></div>
                </div>
            </div>
        </div>
        <?php include('components/events.php'); ?>
        <div class="content">
            <a href="<?php echo $Banner['events_button_link']; ?>" class="big-btn"><?php echo $Banner['events_button_label']; ?><i class="fas fa-arrow-right"></i></a>
        </div>

    </div>
    <?php } ?>


<!--Academy-->
    <?php if(!isset($Banner['academy_display']) || $Banner['academy_display']=="yes"){ ?>
    <div class="services bg-image my-5">
        <div class="image-wrapper">
            <img src="<?php echo $Banner['academy_image']; ?>">
            <div class="overlay"></div>
        </div>
        <div class="services-content d-flex justify-content-center align-items-center">
            <div class="content">
                <div class="w-50 m-auto text-center">
                    <div class="section-title mb-2"><?php echo $Banner['academy_label']; ?></div>
                    <h4 class="font-weight-bold mb-3"><?php echo $Banner['academy_title']; ?></h4>
                </div>
                <?php $home = 'yes';
                include('components/services.php'); ?>
                <div class="text-center mt-5">
                    <a href="<?php echo $Banner['academy_page_link']; ?>" class="big-btn"><?php echo $Banner['academy_button_label']; ?><i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>


<!--    Cloud-->
    <?php if(!isset($Banner['cloud_display']) || $Banner['cloud_display']=="yes"){ ?>
    <div class="content">

        <div class="cloud my-5">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title mb-2"><?php echo $Banner['cloud_label']; ?></div>
                    <h4 class="font-weight-bold mb-3"><?php echo $Banner['cloud_title']; ?></h4>
                    <div class="paragraph t-opacity-75 mb-5"><?php echo nl2br($Banner['cloud_text']); ?></div>
                </div>
            </div>

            <?php include('components/cloud-list.php') ?>

            <a href="<?php echo $Banner['cloud_button_link']; ?>" class="big-btn"><?php echo $Banner['cloud_button_label']; ?><i class="fas fa-arrow-right"></i></a>

        </div>

    </div>
    <?php } ?>

<!--News-->
    <?php  if(isset($c) && $c>0){ if(!isset($Banner['news_display']) || $Banner['news_display']=="yes"){?>
    <div class="news my-5">
        <div class="content">
            <div class="section-title mb-2"><?php echo $Banner['news_label'] ?></div>
            <h4 class="font-weight-bold mb-4"><?php echo $Banner['news_title'] ?></h4>
        </div>

        <div class="slider">
            <div class="owl-carousel">
                <?php foreach ($news as $new){ $newDate = date("d M Y", strtotime($new->event_date));
                    if(isset($new->display_homepage) && $new->display_homepage=="yes"){ ?>
                        <div class="article-card p-4">
                            <label class="section-title mb-2"><?php echo $new->type; ?></label>
                            <h6 class="title mb-2"><?php echo $new->label; ?></h6>
                            <div class="section-title mb-2 date"><?php echo $newDate; ?></div>
                            <a href="<?php echo get_permalink($new->ID); ?>" class="learn-more"><?php echo $Banner['news_button_label']; ?><i class="fas fa-arrow-right"></i></a>
                        </div>
                    <?php } } ?>
            </div>
        </div>
    </div>
    <?php } } ?>


    <?php include('components/contact-us.php') ?>

</div>




<?php get_footer(); ?>
