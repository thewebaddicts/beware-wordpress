<?php

function qsp_theme_support(){
    //adds dynamic tags support
    add_theme_support('title-tag');
    add_theme_support('custom-logo');
}
add_action('after_setup_theme','qsp_theme_support');


function qsp_menu(){

    $locations = [
        'primary' => "Desktop Primary Menu",
        'footer' => "Desktop Footer Menu",
        'footer-2' => "Footer Menu - Quick Links",
        'mobile' => "Mobile Menu"
    ];
    register_nav_menus($locations);
}

add_action('init','qsp_menu');

function qsp_register_styles(){

    wp_enqueue_style('qsp_font-awesome','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css', [], '5.15.1','all');
    wp_enqueue_style('qso_materializecss','https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css', [], '1.0','all');
    wp_enqueue_style('qsp_rellax',get_template_directory_uri().'/assets/js/rellax-master/css/main.css', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('qsp_owlcarousel',get_template_directory_uri().'/assets/js/owl.carousel2/dist/assets/owl.carousel.css', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('qsp_fancybox',get_template_directory_uri().'/assets/js/jquery.fancybox/dist/jquery.fancybox.min.css', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('qsp_main_style',get_template_directory_uri().'/style.css?v=1.2', ['qso_materializecss'], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('qsp_bootstrap',get_template_directory_uri().'/assets/css/grid-system.min.css?v=1.0', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('qsp_material','https://fonts.googleapis.com/icon?family=Material+Icons', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('qsp_aos',get_template_directory_uri().'/assets/js/aos/aos.css', [], wp_get_theme()->get('version'),'all');
//    wp_enqueue_style('qsp_ytplayer_css',get_template_directory_uri().'/assets/js/ytplayer/css/jquery.mb.YTPlayer.min.css', [], '1.0','all');
}

add_action('wp_enqueue_scripts','qsp_register_styles');



function qsp_register_scripts(){
    wp_enqueue_script('qsp_jquery','https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js', array(),'3.5.1',true);
    wp_enqueue_script('qsp_feather','https://unpkg.com/feather-icons', array(),'1.0.0',true);
    wp_enqueue_script('qsp_materializecss_js','https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js', array(),'1.0.0',true);
    wp_enqueue_script('qsp_ytplayer_js',get_template_directory_uri().'/assets/js/youtube-bg/jquery.youtube-background.js', array('qsp_jquery'),'1.0.0',true);
    wp_enqueue_script('qsp_rellax_js',get_template_directory_uri().'/assets/js/rellax-master/rellax.min.js', array(),'1.0.0',true);
    wp_enqueue_script('qsp_owlcarousel_js',get_template_directory_uri().'/assets/js/owl.carousel2/dist/owl.carousel.min.js', array('qsp_jquery'),'1.0.0',true);
    wp_enqueue_script('qsp_fancybox',get_template_directory_uri().'/assets/js/jquery.fancybox/dist/jquery.fancybox.min.js', array('qsp_jquery'),'1.0.0',true);
    wp_enqueue_script('qsp_aos',get_template_directory_uri().'/assets/js/aos/aos.js',array(),'1.0.0',true);
    wp_enqueue_script('qsp_init',get_template_directory_uri().'/assets/js/init.js?v=1.0.3',array('qsp_ytplayer_js','qsp_materializecss_js'),'1.0.2',true);
    wp_enqueue_script('qsp_parallax',get_template_directory_uri().'/assets/js/parallax-js/parallax.min.js',array(),'1.0.0',true);
}

add_action('wp_enqueue_scripts','qsp_register_scripts');




function extractYoutubeID($URL){
    preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $URL, $matches);
    return $matches[0];

}


function extractQueruyValueFrmoURL($param, $URL){
    $parts = parse_url($URL);
    parse_str($parts['query'], $query);
    return $query[$param];
}



function show_page($id) {
    $post = get_post($id);
    $content = apply_filters('the_content', $post->post_content);
    echo $content;
}



function sendmail($subject,$to,$body){
//    print_r($to);exit();
    $content = '<html><head><title>'.$subject.'</title></head><body style="background-color:#FFFFFF;"><div align="center" style="margin-bottom:20px;"><img src="'.get_site_url().'/wp-content/themes/beware/assets/images/logo.png" width="200" border="0" style="margin:auto;" /></div><div style="margin:auto; background-color:rgba(0,0,0,0.05);  max-width:560px; width:100%; clear:both; padding:20px; font-family:Hind, sans-serif; font-size:14px; color:#000000; text-align:left; margin-bottom:20px; ">'.$body.'</div>';
    $content .= '</div></body></html>';

    date_default_timezone_set('Etc/UTC');

    require_once 'PHPMailer-master/PHPMailerAutoload.php';

    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->SMTPSecure = "tls";
//    $mail->SMTPDebug = 1; //1 for debugging
    $mail->Host = "mail.smtp2go.com";
    $mail->Port = "2525";
    $mail->SMTPAuth = true;
    $mail->Username = "noreply@thewebaddicts.com";  // SMTP username
    $mail->Password = "V2NEDPsaTh1i"; // SMTP password


    $mail->Subject = $subject;
    $mail->setFrom("noreply@thewebaddicts.com", "Beware"); //**Write here sender email. For example, emails will be sent to you from your website, so write email of your website (if you don't have it, write any email, which you want) and the name of your website. Example ('email@your-website.com', 'your-website.com')) Send from a fixed, valid address in your own domain, perhaps one that allows you to easily identify that it originated on your contact form**
    $mail->addAddress($to); //**WRITE HERE RECIPIENT EMAIL ADDRESS (AT THIS ADDRESS EMAILS WILL BE COME)**

    if ($mail->addReplyTo($to, $to)) {
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';
        //Build a simple message body

        $mail->Body = $content;
        //Send the message, check for errors
        if(!$mail->Send()) {
            $arrResult = array ('response'=>'error');
        }

        $arrResult = array ('response'=>'success');
        echo "<div style='font-size:14px;font-weight:500;'> Your form has been submitted successfully! </div>";
    } else {
        $arrResult = array ('response'=>'error');
        echo json_encode($arrResult);

    }

}


