

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>
<body>
<?php $TopInfo = get_fields('2'); ?>
<?php $page = get_post_meta(get_the_ID()); ?>


<header class="main_menu" id="main_menu">

    <nav>
        <?php if(isset($TopInfo["header_title"]) && $TopInfo['header_title']!=''){ ?>
        <div class="header-top-title">
            <div class="content">
                <div class="title"><?php echo $TopInfo['header_title']; ?></div>
            </div>
        </div>
        <?php } ?>

        <div class="content">

            <a href="#" class="toggle"><i class="fas fa-bars mobile-burgers"></i><i class="fas fa-times"></i></a>
            <?php if(function_exists('the_custom_logo')){ the_custom_logo(); } ?>
            <?php
            wp_nav_menu([
                "menu" => "primary",
                "theme_location" => "primary",
                "container" => "",
                "items_wrao" => "<ul></ul>",
            ]);




            wp_nav_menu([
                "menu" => "primary",
                "theme_location" => "primary",
                "container" => "",
                "items_wrao" => "<ul></ul>",
                "menu_class" => "mobile"
            ]);
            ?>
        </div>
    </nav>
</header>



<?php if(is_admin_bar_showing()){ ?>
<style>
    header.main_menu{ top:30px !important; }
</style>
<?php } ?>
