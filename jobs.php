<div class="menu-spacer"></div>
<?php /* Template Name: Job posts */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>

<?php $page = get_fields(get_the_ID());?>
<?php global $post;  ?>

<?php

if(isset($_GET['date'])){
    $filterDate = $_GET['date'];
}else{
    $filterDate = "ASC";
}


if(isset($_GET['position']) && $_GET['position']!="all"){
    $filterTitle =  array(
        'key'          => 'label',
        'value'        => $_GET['position'],
        'compare'      => '=',
    );
}else{
    $filterTitle = "";
}
if(isset($_GET['type'])){
    $filterType = array(
        'key'          => 'type',
        'value'        => $_GET['type'],
        'compare'      => '=',
    );
}else{
    $filterType = "";
}
$featured_jobs= get_posts( array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'category_name' => 'jobs',
    'posts_per_page'=>-1,
    'orderby'          => 'date',
    'order'            => $filterDate,
    'meta_query'       => array(
        'relation'    => 'AND',
        $filterTitle,
        $filterType,
    ),
));


?>


<div class="jobs">

    <div class="page-banner" style="background-image: url('<?php echo $page['background_image']; ?>')">
        <div class="overlay"></div>
    </div>

    <div class="content">

        <div class="pb-5">

            <h3 class="font-weight-bold mb-3"><?php echo $post->post_title; ?></h3>

            <div class="breadcrumbs mb-5">
                <a href="/"><span>Homepage</span></a>
                <span class="mx-2">/</span>
                <a href="jobs.php"><span><?php echo $post->post_title; ?></span></a>
            </div>




            <div class="filters-row row mb-5">
                <div class="col-lg-6">
                    <div class="d-flex align-items-center flex-wrap">
                        <div class="title-filter filter-inner mr-3 mb-2" data-aos="fade-up" data-aos-delay="75">
                            <select class="browser-default fix-select bg-black" onchange="changeQueryParameter('position',$(this).val())" >
                                <option value="" <?php if (!isset($_GET["title"])){?> selected  <?php  } ?> disabled><?php echo $page['job_position_label']; ?></option>
                                <option value="all" <?php if (isset($_GET["position"]) && $_GET["position"] == 'all'){?> selected  <?php  } ?>>All</option>

                                <?php
                                $jobs = get_posts( array(
                                    'post_type' => 'post',
                                    'post_status' => 'publish',
                                    'category_name' => 'jobs',
                                    'posts_per_page'=>-1,
                                    'orderby'          => 'date',
                                    'order'            => 'ASC',
                                ));
                                $jobsFiltered = array();
                                if($jobs){
                                    foreach ($jobs as $job){
                                        array_push($jobsFiltered, $job->label);
                                    }
                                }
                                $jobs = array_unique($jobsFiltered);
                                if($jobs){
                                    foreach ($jobs as $job){
                                        ?>
                                        <option value="<?php echo $job ?>" <?php if (isset($_GET["position"]) && $_GET["position"] == $job){?> selected  <?php  } ?>><?php echo $job?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="type-filter filter-inner mb-2" data-aos="fade-up" data-aos-delay="100">
                            <select class="browser-default fix-select bg-black" onchange="changeQueryParameter('type',$(this).val())">
                                <option value="" <?php if (!isset($_GET["type"])){?> selected  <?php  } ?> disabled><?php echo $page['job_type_label']; ?></option>
                                <option value="Full Time" <?php if (isset($_GET["type"]) && $_GET["type"] == "Full Time"){?> selected  <?php  } ?>><?php echo $page['full_time_label']; ?></option>
                                <option value="Part Time" <?php if (isset($_GET["type"]) && $_GET["type"] == "Part Time"){?> selected  <?php  } ?>><?php echo $page['part_time_label']; ?></option>
                            </select>
                        </div>

                    </div>
                </div>


                <div class="col-lg-6 d-flex align-items-center justify-content-lg-end">
                    <div class="date-filter filter-inner" data-aos="fade-up" data-aos-delay="50">
                        <select class="browser-default fix-select with-border" onchange="changeQueryParameter('date',$(this).val())">
                            <option value="" disabled <?php if (!isset($_GET["date"])){?> selected  <?php  } ?>><?php echo $page['sort_by_label']; ?></option>
                            <option value="DESC" <?php if (isset($_GET["date"]) && $_GET["date"] == "DESC"){?> selected  <?php  } ?>>Newest to Oldest</option>
                            <option value="ASC" <?php if (isset($_GET["date"]) && $_GET["date"] == "ASC"){?> selected  <?php  } ?>>Oldest to Newest</option>
                        </select>
                    </div>
                </div>

            </div>


            <?php if(isset($featured_jobs) && sizeof($featured_jobs)>0){ ?>

                <?php foreach ($featured_jobs as $job){ $newDate = date("d M Y", strtotime($job->display_date));  ?>
                <div class="job-div my-4">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <div class="d-flex align-items-center">
                            <div class="section-title mr-4 text-capitalize"><?php echo $job->type; ?></div>
                            <a target="_blank" href="<?php $job->location_url; ?>" class="location d-flex align-items-center"><i class="fas fa-map-marker-alt mr-1"></i><?php echo $job->location; ?></a>
                        </div>
                        <div class="section-title date"><?php echo $newDate; ?></div>
                    </div>
                    <h6 class="font-weight-bold mb-3"><?php echo $job->label; ?></h6>
                    <div class="paragraph t-opacity-75 mb-4">
                        <?php echo nl2br( $job->description); ?>
                    </div>
                    <a href="javascript:;" onclick="FillApplication('<?php echo $job->label; ?>')" class="learn-more pb-3 d-inline-block">Apply Now<i class="fas fa-arrow-right"></i></a>

                </div>
                <?php } ?>

            <?php }else{ ?>
                No jobs found
            <?php } ?>




            <div class="contact-form d-flex justify-content-center py-5" id="applyjob_form">
                <form action="" method="POST" class="w-50" enctype="multipart/form-data">
                    <div class="w-50 m-auto text-center mb-3">
                        <h4 class="font-weight-bold mb-4"><?php echo $page['form_title']; ?></h4>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-6 mb-3">
                            <div class="input-div">
                                <div class="form-label"><?php echo $page['first_name']['label']; ?></div>
                                <input class="form-input" type="text" name="first_name" placeholder="<?php echo $page['first_name']['placeholder']; ?>" required>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="input-div">
                                <div class="form-label"><?php echo $page['last_name']['label']; ?></div>
                                <input class="form-input" type="text" name="last_name" placeholder="<?php echo $page['last_name']['placeholder']; ?>" required>
                            </div>
                        </div>
                        <div class="col-lg-12 mb-3">
                            <div class="input-div">
                                <div class="form-label"><?php echo $page['email_address']['label']; ?></div>
                                <input class="form-input" type="email" name="email" placeholder="<?php echo $page['email_address']['placeholder']; ?>" required>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="input-div">
                                <div class="form-label"><?php echo $page['job_position']['label']; ?></div>
<!--                                <input class="form-input h-100" name="job_position" id="apply_position" placeholder="--><?php //echo $page['job_position']['placeholder']; ?><!--">-->
                                <select class="form-input h-100" name="job_position" id="apply_position" style="display: block !important;" required>
                                    <option value="" disabled selected>Job Position</option>
                                    <?php if(isset($featured_jobs) && sizeof($featured_jobs)>0){ ?>
                                    <?php foreach ($featured_jobs as $job){ ?>
                                    <option value="<?php echo $job->label ?>"><?php echo $job->label ?></option>
                                        <?php } ?>
                                    <?php } ?>

                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="input-div">
                                <label for="cv" class="cv-upload w-100 mb-0 d-flex justify-content-between align-items-center form-input"><?php echo $page['upload_cv']['label']; ?><i data-feather="upload" width="16"></i></label>
                                <input id="cv" type="file" name="attachment" hidden required>
                            </div>
                        </div>
                        <div class="col-lg-12 mb-3">
                            <div class="input-div">
                                <div class="form-label"><?php echo $page['motivational_letter']['label']; ?></div>
                                <textarea class="form-input" type="text" name="message" placeholder="<?php echo $page['motivational_letter']['placeholder']; ?>" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center align-items-center">
                        <button type="submit" name="submit" class="big-btn"><?php echo $page['button_label']; ?><i class="fas fa-arrow-right"></i></button>
                    </div>


                    <?php

                    //                $my_custom_filename = time() . $_FILES['attachment']['name'];
                    //                wp_upload_bits($my_custom_filename , null, file_get_contents($_FILES['fileToUpload']['tmp_name']));

                    $postData = $uploadedFile = $statusMsg = '';
                    $msgClass = 'errordiv';
                    if(isset($_POST['submit'])){
                        // Get the submitted form data
                        $postData = $_POST;
                        $email = $_POST['email'];
                        $position = $_POST['job_position'];
                        $name = $_POST['first_name'].' '.$_POST['last_name'];
                        $subject = 'New Career From Submit';
                        $message = $_POST['message'];

                        // Check whether submitted data is not empty
                        if(!empty($email) && !empty($name) && !empty($subject) && !empty($message)){

                            // Validate email
                            if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
                                echo 'Please enter your valid email.';
                            }else{
                                $uploadStatus = 1;

                                // Upload attachment file
                                if(!empty($_FILES["attachment"]["name"])){

                                    // File path config
                                    $targetDir = "wp-content/uploads/files/";
                                    $fileName = time() . $_FILES['attachment']['name'];
                                    $targetFilePath = $targetDir . $fileName;
                                    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

                                    // Allow certain file formats
                                    $allowTypes = array('pdf', 'doc', 'docx');
                                    if(in_array($fileType, $allowTypes)){
                                        // Upload file to the server
                                        if(move_uploaded_file($_FILES["attachment"]["tmp_name"], $targetFilePath)){
                                            $uploadedFile = $targetFilePath;
                                        }else{
                                            $uploadStatus = 0;
                                            echo "Sorry, there was an error uploading your file.";
                                        }
                                    }else{
                                        $uploadStatus = 0;
                                        echo 'Sorry, only PDF and DOC files are allowed to upload.';
                                    }
                                }

                                if($uploadStatus == 1){

                                    // Recipient
                                    $toEmail = trim($page['send_form_to_email']);

                                    // Subject
                                    $emailSubject = 'Career form new submit';

                                    // Message
                                    $htmlContent = '<h2>Career Form Submitted</h2>
                    <p><b style="color:#00A48E">Full Name:</b> '.$name.'</p>
                    <p><b style="color:#00A48E">Email:</b> '.$email.'</p>
                    <p><b style="color:#00A48E">Job Position:</b> '.$position.'</p>
                    <p><b style="color:#00A48E">Message:</b><br/>'.$message.'</p>';

                                    // Header for sender info

                                    if(!empty($uploadedFile) && file_exists($uploadedFile)){


                                        // Multipart boundary
                                        $message = "\n\n" . $htmlContent . "\n\n";

                                        // Preparing attachment
                                        if(is_file($uploadedFile)){
                                            $fp =    @fopen($uploadedFile,"rb");
                                            $data =  @fread($fp,filesize($uploadedFile));
                                            @fclose($fp);
                                            $data = chunk_split(base64_encode($data));
                                            $message .= '<b style="color:#00A48E">CV file:</b> <a href="'.get_site_url().'/'.$targetFilePath.'">'.$fileName.'</a>';
                                        }




                                        // Send email
                                        sendmail($emailSubject, $toEmail, $message);

                                        // Delete attachment file from the server
//                    @unlink($uploadedFile);
                                    }else{
                                        // Set content-type header for sending HTML email
                                        $headers .= "\r\n". "MIME-Version: 1.0";
                                        $headers .= "\r\n". "Content-type:text/html;charset=UTF-8";

                                        // Send email
                                        sendmail($emailSubject, $toEmail, $htmlContent);
                                    }

                                }
                            }
                        }else{
                            $statusMsg = 'Please fill all the fields.';
                        }
                    }




                    ?>


                </form>
            </div>

        </div>

    </div>

</div>





<?php get_footer(); ?>


