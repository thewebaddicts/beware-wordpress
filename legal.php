<div class="menu-spacer"></div>
<?php /* Template Name: Legal Informative  */ ?>
<?php
get_header();
$page = get_fields(get_the_ID());


?>

<div class="content">

    <div class="informative-page">
        <h4 class="title"><?php echo $page['title']; ?></h4>
        <div class="text">
            <?php echo nl2br($page['text']); ?>

        </div>


    </div>

</div>



<?php get_footer(); ?>


