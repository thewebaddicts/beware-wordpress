<div class="menu-spacer"></div>
<?php /* Template Name: News & Events */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>

<?php
//$page = get_fields('84');
$page = get_fields(get_the_ID());


if(isset($_GET['order_date'])){
    $filterDate = $_GET['order_date'];
}else{
    $filterDate = "ASC";
}
$articles = get_pages([
        "child_of" => get_the_ID(),
]);

if(isset($_GET['order_date']) && $_GET['order_date']=="DESC" ){
    usort($articles, function($a, $b) {
        return strtotime($a->event_date) - strtotime($b->event_date);
    });
}else{
    usort($articles, function($a, $b) {
        return strtotime($b->event_date) - strtotime($a->event_date);
    });
}

?>
<?php global $post;  ?>


<div class="news">

    <div class="page-banner" style="background-image: url('<?php echo $page['background_image']; ?>');">
        <div class="overlay"></div>
    </div>

    <div class="content">

        <div class="pb-5">

            <h3 class="font-weight-bold mb-3"><?php echo $post->post_title; ?></h3>

            <div class="breadcrumbs mb-5">
                <a href="/"><span>Homepage</span></a>
                <span class="mx-2">/</span>
                <a><span><?php echo $post->post_title; ?></span></a>
            </div>

            <div class="d-md-flex justify-content-between align-items-center py-5">
                <div class="sort-div mb-md-0 mb-3">
                    <select class="fix-select with-border browser-default" onchange="changeQueryParameter('order_date',$(this).val(),'section1')">
                        <option value="" disabled <?php if (!isset($_GET["order_date"])){?> selected  <?php  } ?>>Sort by</option>
                        <option value="DESC" <?php if (isset($_GET["order_date"]) && $_GET["order_date"] == "DESC"){?> selected  <?php  } ?>> Newest to Oldest </option>
                        <option value="ASC" <?php if (isset($_GET["order_date"]) && $_GET["order_date"] == "ASC"){?> selected  <?php  } ?>>Oldest to Newest</option>
                    </select>
                </div>


                <div class="search-div d-flex align-items-center py-2 px-3">
                    <input type="text" name="query" class="search-input w-100 p-2" placeholder="Search..." onkeyup="searchArticle()">
                    <i data-feather="search" width="24px"></i>
                </div>
            </div>

            <div class="articles-list">
                <div class="row articles-row">
                    <?php if(isset($articles) && sizeof($articles)>0){
                        foreach ($articles as $article){
                            $newDate = date("d M Y", strtotime($article->event_date)); ?>
                    <div class="col-lg-4 mb-4 article-card-container">
                        <div class="article-card w-100 h-100 p-4">
                            <div class="section-title mb-2"><?php echo $article->type; ?></div>
                            <h5 class="title mb-2"><?php echo $article->label; ?></h5>
                            <div class="mb-2 date" style="text-transform: uppercase"><?php echo $newDate; ?></div>
                            <a href="<?php echo get_permalink($article->ID); ?>" class="learn-more">Read More<i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                    <?php }} ?>

                </div>
            </div>

        </div>

    </div>

</div>

<!---->
<!---->
<?php // $pages = get_pages([ "child_of" => get_the_ID(), "parent" => get_the_ID(), "sort_column" => "menu_order" ]); ?>
<!---->
<?php
//foreach ($pages AS $page){
//    $html = file_get_contents($page->guid); echo $html;
//} ?>





<?php get_footer(); ?>


