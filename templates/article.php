<div class="menu-spacer"></div>
<?php /* Template Name: News Article */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>

<?php $page = get_fields(get_the_ID());?>
<?php global $post;  ?>

<div class="container">

    <div class="article-details m-auto">

        <div class="p-4">
            <div class="text-center mb-4">
                <div class="breadcrumbs mb-3">
                    <a href="/"><span>Homepage</span></a>
                    <span class="mx-2">/</span>
                    <a><span><?php echo get_the_title($post->post_parent); ?></span></a>
                    <span class="mx-2">/</span>
                    <a><span><?php echo $page['type']; ?></span></a>
                </div>

                <h4 class="text-center mb-3"><?php echo $page['label']; ?></h4>

                <div class="section-title date"><?php echo $page['event_date']; ?></div>
            </div>

            <img class="img" src="<?php echo $page['image']; ?>">

            <div class="paragraph text-justify my-5">
                <?php echo nl2br($page['text']); ?>
            </div>

            <div class="d-flex justify-content-center mb-5">
                <div class="addthis_inline_share_toolbox_musc"></div>
            </div>

        </div>

    </div>

</div>

<?php  $pages = get_pages([ "child_of" => get_the_ID(), "parent" => get_the_ID(), "sort_column" => "menu_order" ]); ?>

<?php
foreach ($pages AS $page){
    $html = file_get_contents($page->guid); echo $html;
} ?>





<?php get_footer(); ?>


